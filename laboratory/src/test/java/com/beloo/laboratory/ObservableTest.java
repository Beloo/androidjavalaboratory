package com.beloo.laboratory;

import com.beloo.laboratory.util.TestRunnable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.AbstractList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.PublishSubject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

@RunWith(JUnit4.class)
public class ObservableTest {

    public static class TestList<T> extends AbstractList<T> {

        @Override
        public T get(int index) {
            throw new IllegalStateException();
        }

        @Override
        public int size() {
            return 2;
        }

        @Override
        public boolean add(T t) {
            return true;
        }
    }

    @Test
    public void toList_ObservableCreatedFromListAndWithoutOtherModifiers_DoNotIterate() {
        //arrange
        TestList<Object> testList = new TestList<>();
        testList.add(new Object());
        testList.add(new Object());
        TestObserver subscriber = new TestObserver();

        //act
        Observable observable = Observable.just(testList);
        //noinspection unchecked
        observable.subscribe(subscriber);
        subscriber.awaitTerminalEvent();

        //assert
        subscriber.assertNoErrors();
    }

    @Test
    public void toList_generatesEmptyListInCaseEmptySequence() throws Exception {
        //arrange
        Observable<Object> observable = Observable.empty();
        TestObserver<List<Object>> subscriber = new TestObserver<>();

        //act
        observable.toList().subscribe(subscriber);

        //assert
        subscriber.assertValueCount(1);
        List<Object> objects = subscriber.values().get(0);
        assertEquals(0, objects.size());
    }

    @Test
    public void to_AfterMerge_NotSubscribedOnNestedObservables() throws Exception {
        //arrange
        TestRunnable testRunnable1 = new TestRunnable();
        TestRunnable testRunnable2 = new TestRunnable();

        Completable complete1 = Completable.fromCallable(Object::new).doOnSubscribe(s -> testRunnable1.run());
        Completable complete2 = Completable.fromCallable(Object::new).doOnSubscribe(s -> testRunnable1.run());
        TestObserver<Object> testSubscriber = TestObserver.create();

        Object value = new Object();
        Single<Object> single = Single.just(value);

        //act
        Completable.mergeArray(complete1, complete2).to(c -> single).subscribe(testSubscriber);

        //assert
        testSubscriber.assertComplete();
        testSubscriber.assertValue(value);
        assertFalse(testRunnable1.getWasRun());
        assertFalse(testRunnable2.getWasRun());
    }

    @Test
    public void subscribed_ExceptionInOnErrorObserver_DoOnTerminateNotCalled() throws CompositeException {
        //arrange
        Observable<Object> observable = Observable.just(1);
        TestRunnable testRunnable = new TestRunnable();

        //act
        try {
            observable.doOnTerminate(testRunnable::run).subscribe(ev -> {
                throw new IllegalArgumentException();
            }, err -> {
                throw new IllegalStateException();
            });
        } finally {
            //assert
            assertFalse(testRunnable.getWasRun());
        }

    }

    @Test
    public void subscribed_EventSynchronouslySent() throws Exception {
        //arrange
        PublishSubject<String> subject = PublishSubject.create();
        Observable<String> observable = subject.toSerialized();
        TestObserver<String> testObserver = TestObserver.create();
        observable.subscribe(testObserver);
        for(int i=0; i < 10000; i++) {
            subject.onNext("test");
        }
        testObserver.assertValueCount(10000);
    }

}
