package com.beloo.laboratory

import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import junit.framework.Assert.assertTrue
import org.junit.Test
import java.util.concurrent.TimeUnit

class OnErrorCompleteTest {

    @Test
    @Throws(Exception::class)
    fun `onErrorComplete on Exception terminates stream with completion`() {

        val values = mutableListOf<Long>()
        val lowRange = Observable.intervalRange(1, 10, 0, 5, TimeUnit.MILLISECONDS)
        val highRange = Observable.intervalRange(100, 10, 10, 10, TimeUnit.MILLISECONDS)

        //arrange
        val sourceObservable = Observable
                .combineLatest(
                        lowRange.doOnNext {
                            if (it == 3L) throw java.lang.Exception("Boom!")
                        },
                        highRange,
                        BiFunction { t1: Long, t2: Long -> t1 + t2 }
                )
                .doOnNext { values.add(it) }
                .ignoreElements()
                .onErrorComplete()

        val testObserver = sourceObservable.test()

        testObserver.awaitTerminalEvent()
        println(values)
        assertTrue(values.size == 1)
    }
}
