package com.beloo.laboratory

import io.reactivex.Observable
import org.junit.Test
import java.util.concurrent.TimeUnit

class ShareTest {

    @Test
    fun `share does not work with cold observable`() {

        val observable = Observable.just(Unit)
                .delaySubscription(1, TimeUnit.SECONDS)
                .doOnNext {
                    println("test")
                }
                .share()

        Observable.merge(
                observable,
                observable
        ).test().awaitTerminalEvent()
    }
}