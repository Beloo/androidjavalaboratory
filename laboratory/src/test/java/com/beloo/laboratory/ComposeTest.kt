package com.beloo.laboratory

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.junit.Test

class ComposeTest {

    @Test
    fun `compose should not duplicate upstream events`() {
        val subject = PublishSubject.create<Unit>()

        fun <D> testFun(observable: Observable<Unit>): Observable<D> = observable
                .doOnNext { println("onNext") }
                .compose {
                    Observable.empty<D>()
                }

        testFun<Any>(Observable.just(Unit))
                .test()
                .awaitTerminalEvent()
    }
}