package com.beloo.laboratory;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.ReplaySubject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class HotSubscriber {

    //hot subject
    private ReplaySubject<Integer> transferSubject;

    @Before
    public void setUp() throws Exception {
        transferSubject = ReplaySubject.create(1);
    }

    @Test
    public void subscribeOnSubject_dataUpdatedByEvent_receivedOldAndNewData() throws Exception {

        //arrange
        //event observable
        Observable<Boolean> eventObservable = Observable.just(true)
                .delay(250, TimeUnit.MILLISECONDS);

        eventObservable.subscribe(bool -> query());

        TestObserver<Integer> testObserver = new TestObserver<>();

        //act
        transferSubject.subscribe(testObserver);
        query();
        Thread.sleep(500);

        //assert
        System.out.println(testObserver.values());
        assertEquals(6, testObserver.values().size());
    }

    private void query() {
        System.out.println("query");
        //data observable
        Observable<Integer> dataObservable = Observable.range(0, 3);
        dataObservable.subscribe(item -> transferSubject.onNext(item));
    }
}
