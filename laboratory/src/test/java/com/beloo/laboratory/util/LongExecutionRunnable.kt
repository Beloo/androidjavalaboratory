package com.beloo.laboratory.util

class LongExecutionRunnable : Runnable {
    override fun run() {
        try {
            Thread.sleep(500)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }
}
