package com.beloo.laboratory.util

class TestRunnable : Runnable {
    var wasRun = false

    override fun run() {
        wasRun = true
    }
}
