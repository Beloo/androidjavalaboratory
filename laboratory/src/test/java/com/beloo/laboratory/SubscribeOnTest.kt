package com.beloo.laboratory

import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert
import org.junit.Before
import org.junit.Test

class SubscribeOnTest {


    private fun printThreadName(tag: String): String {
        val threadName = Thread.currentThread().name
        println("$tag: currentThread = $threadName")
        return threadName
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
    }

    @Test
    @Throws(Exception::class)
    fun subscribeOn_SubscribedToYThreadInFlatMap_NextOperatorsCalledOnTheadY() {

        //arrange
        val sourceObservable = Observable.just("item")
            .subscribeOn(Schedulers.single())

        val testObserver = TestObserver<String>()
        var starterThreadName = ""
        var resultThreadName = ""

        sourceObservable
            .doOnNext {
                starterThreadName = printThreadName("doOnNext, started")
            }
            .flatMap {
                Observable.just("item2")
                    .subscribeOn(Schedulers.computation())
            }
            .doOnNext {
                resultThreadName = printThreadName("doOnNext, changed")
            }
            .subscribeOn(Schedulers.newThread())
            .doOnNext {
                resultThreadName = printThreadName("doOnNext, end")
            }
            .subscribe(testObserver)
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        Assert.assertTrue(starterThreadName != resultThreadName)
    }

}
