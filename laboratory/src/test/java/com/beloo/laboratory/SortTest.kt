package com.beloo.laboratory

import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.ArrayList
import kotlin.Comparator

class SortTest {

    @Test
    fun sortWithComparator_MoveOneItemUp_ChangesInitialCollection() {
        val list = ArrayList(listOf(1, 2, 3, 4, 5, 6))

        list.sortWith(Comparator { o1, o2 -> if (o1 == 5) -1 else 0 })

        assertEquals(listOf(5, 1, 2, 3, 4, 6), list)
    }


}
