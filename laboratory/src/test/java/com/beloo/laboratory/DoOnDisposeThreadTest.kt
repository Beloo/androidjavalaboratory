package com.beloo.laboratory

import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert
import org.junit.Before
import org.junit.Test

class DoOnDisposeThreadTest {


    private fun printThreadName(tag: String): String {
        val threadName = Thread.currentThread().name
        println("$tag: currentThread = $threadName")
        return threadName
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
    }

    @Test
    @Throws(Exception::class)
    fun doFinally_ThreadChangedInChainAndPreviouslyCalledObserveOnStarterThread_CalledOnStartedThread() {

        //arrange
        val sourceObservable = Observable.just("item")
            .subscribeOn(Schedulers.single())

        val testObserver = TestObserver<String>()
        var starterThreadName = ""
        var doFinallyThreadName = ""

        sourceObservable
            .doOnNext {
                starterThreadName = printThreadName("doOnNext, started")
            }
            .observeOn(Schedulers.computation())
            .doOnNext {
                printThreadName("doOnNext, changed")
            }
            .doFinally {
                printThreadName("doFinally, middle")
            }
            .observeOn(Schedulers.single())
            .doOnNext {
                printThreadName("doOnNext")
            }
            .doFinally {
                doFinallyThreadName = printThreadName("doFinally, end")
            }
            .observeOn(Schedulers.newThread())
            .subscribe(testObserver)
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        Assert.assertEquals(starterThreadName, doFinallyThreadName)
    }

}
