package com.beloo.laboratory

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import org.junit.Test
import java.util.concurrent.TimeUnit

const val FIRING_RATE_IN_SEC = 3L

class PauseAndResumeObservableTest {

    enum class TimeOfDay {
        DAWN,
        AFTERNOON,
        DUSK,
        UNKNOWN
    }

    enum class InterruptEvent {
        PAUSE, RESUME
    }

    data class Ammo(val caliber: Float)

    // This is a repeating 24 sec day stream.
// Dawn happens at 5
// Afternoon at 12
// Dusk at 18
    fun timeOfDayStream(): Observable<TimeOfDay> {
        return Observable.interval(1, TimeUnit.SECONDS)
                .map { (it % 24) + 1 }
                .map {
                    when (it) {
                        5L -> TimeOfDay.DAWN
                        12L -> TimeOfDay.AFTERNOON
                        18L -> TimeOfDay.DUSK
                        else -> TimeOfDay.UNKNOWN
                    }
                }.filter { it != TimeOfDay.UNKNOWN }
    }

    fun duskStream(): Observable<TimeOfDay> {
        return timeOfDayStream().filter{tod ->
            tod == TimeOfDay.DUSK
        }
    }

    // A warship that fire one ammo every "firingDelay" seconds.
    fun warShip(firingDelay: Long): Observable<Ammo> {
        return Observable.interval(firingDelay, TimeUnit.SECONDS)
                .map{Ammo(0.51f)}
    }

    fun fireAndStopAtDusk(): Observable<Ammo> {
        val warship = warShip(FIRING_RATE_IN_SEC)
                .takeUntil(duskStream())
        return Observable.merge(warship, watchTower())
    }

    // Converts timeStream to pause and resume stream
// Pause at Dusk and Resume at Dawn
    fun pauseAndResumeStream(): Observable<InterruptEvent> {
        return timeOfDayStream()
                .filter { it == TimeOfDay.DUSK || it == TimeOfDay.DAWN }
                .map {
                    if (it == TimeOfDay.DUSK) InterruptEvent.PAUSE
                    else InterruptEvent.RESUME
                }
    }

    class PauseResumeTransformer<T>(private val interruptEvent: Observable<InterruptEvent>) : ObservableTransformer<T, T> {
        override fun apply(source: Observable<T>): ObservableSource<T> {
            return Observable.switchOnNext(interruptEvent.map {
                if(it == InterruptEvent.RESUME) source
                else Observable.never()
            })
        }
    }

    fun pauseResumeWarship(interruptEvent: Observable<InterruptEvent>): Observable<Ammo> {
        val warship = warShip(FIRING_RATE_IN_SEC)
                .compose(PauseResumeTransformer<Ammo>(interruptEvent))
        return Observable.merge(warship, watchTower())
    }

    class FriendlyFireException(message: String): Exception(message)

    // WatchTower which emits nothing but FriendlyFireException if a friendly
// unit is spotted in firing zone.
    fun watchTower(): Observable<Ammo>{
        return Observable.interval(1, TimeUnit.SECONDS)
                .map { Math.random() }
                // This watchTower has 3% prob of firing a friendly unit exception
                .filter { it > .97 }
                .map { throw FriendlyFireException("Friendly unit spotted") }
    }

    @Test
    fun testPauseAndResume() {

        Thread {
            pauseResumeWarship(pauseAndResumeStream())
                    .doOnNext { println(it) }
                    .subscribe()

        }.start()

        Thread.sleep(100000)

    }
}
