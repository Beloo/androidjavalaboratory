package com.beloo.laboratory

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

class RetryTest {

    @Test
    fun retry_AfterReplay_retryResubscribeToSource() {

        //arrange
        val sourceObservable = Observable.intervalRange(1, 3, 10, 10, TimeUnit.MILLISECONDS)
            .concatWith(Observable.error(InterruptedException("interrupted")))
            .doOnError { println("interrupted") }

        val replay = sourceObservable.replay(1)

        val retry = replay
            .doOnNext { println("item: $it") }
            .retry(2)

        val testObserver = TestObserver<Long>()
        retry.subscribe(testObserver)
        replay.connect()
        testObserver.awaitTerminalEvent()
    }

    @Test
    fun `retry should resubscribe to fromCallable`() {
        class DeferSource {
            private var value = 1
            fun getValue(): Int = value++
        }
        val deferSource = DeferSource()

        val observer = Single.fromCallable { deferSource.getValue() }
                .concatWith(Single.error(InterruptedException("interrupted")))
                .doOnError { println("interrupted") }
                .retry(2)
                .doOnNext { println("item: $it") }
                .test()

        observer.assertValues(1, 2, 3)
    }

}
