package com.beloo.laboratory;

import com.beloo.laboratory.util.LongExecutionRunnable;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.*;

/**
 * Test which shows parallel streaming with rxJava
 */
public class RxJavaComputationInParallelTest {

    private long startMillis;
    private Service service;

    @Before
    public void setUp() throws Exception {
        startMillis = System.currentTimeMillis();
        service = new Service();

        int cores = Runtime.getRuntime().availableProcessors();
        Assume.assumeTrue(cores >= 2);
    }

    @Test
    public void parallelStreaming_ConsumeApproximately3SecondsSynchronously_performsAtLeastTwoTimesFaster() throws Exception {
        //arrange
        Event event = new Event(1);
        TestObserver<Facility> testSubscriber = new TestObserver<>();

        //act
        Observable.fromIterable(event.facilityIds)
                //or just .map(service::getFacility)
                .flatMap(facilityId -> Observable.fromCallable(() -> service.getFacility(facilityId))
                        .subscribeOn(Schedulers.computation()))
                .doOnNext(facility -> event.facilities.add(facility))
                .subscribe(testSubscriber);

        testSubscriber.awaitTerminalEvent();
        long consumedTime = System.currentTimeMillis() - startMillis;

        //assert
        testSubscriber.assertNoErrors();

        System.out.println(consumedTime);
        System.out.println(event.facilities);

        assertTrue(event.facilities.size() > 0);
        assertTrue(consumedTime < 1500);
    }

    @Test
    public void parallelStreaming_TwoEventsEachEventConsumeApproximately3SecondsSynchronously_performsAtLeastTwoTimesFaster() throws Exception {
        //arrange
        Event event1 = new Event(1);
        Event event2 = new Event(2);

        TestObserver<Event> testSubscriber = new TestObserver<>();

        //act
        Observable.fromArray(event1, event2)
                .subscribeOn(Schedulers.io())
        .doOnNext(event -> Observable.fromIterable(event.facilityIds)
                //or just .map(service::getFacility)
                .flatMap(facilityId -> Observable.fromCallable(() -> service.getFacility(facilityId))
                        .subscribeOn(Schedulers.computation()))
                .blockingSubscribe(facility -> event.facilities.add(facility)))
                .subscribe(testSubscriber);

        testSubscriber.awaitTerminalEvent();
        long consumedTime = System.currentTimeMillis() - startMillis;
        testSubscriber.assertNoErrors();

        //assert
        System.out.println(consumedTime);
        System.out.println(testSubscriber.getEvents());
        assertTrue(consumedTime < 3000);
    }

    private class Event {
        int id;

        Event(int id) {
            this.id = id;
        }

        List<Integer> facilityIds = Arrays.asList(1,2,3,4,5,6);
        List<Facility> facilities = new LinkedList<>(); // Empty, the one I need to load

        @Override
        public String toString() {
            return "event id = " + id + " facilities = " + facilities.toString();
        }
    }

    private class Facility {
        int id;

        Facility(int num) {
            this.id = num;
        }

        @Override
        public String toString() {
            return "facility id = " + id;
        }
    }

    private class Service {
        Facility getFacility(int id) {
            new LongExecutionRunnable().run();
            return new Facility(id);
        }
    }
}