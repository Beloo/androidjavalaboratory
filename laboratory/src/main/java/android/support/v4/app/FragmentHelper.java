package android.support.v4.app;

public class FragmentHelper {

    public static boolean isInBackStack(Fragment fragment) {
        return fragment.isInBackStack();
    }

}
