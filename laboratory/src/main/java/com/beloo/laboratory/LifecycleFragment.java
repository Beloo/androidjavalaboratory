package com.beloo.laboratory;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LifecycleFragment extends Fragment {

    @BindView(R.id.tvTag)
    TextView tvTag;

    private void printState() {
        Log.d(getTag(), "isAdded = " + isAdded());
        Log.d(getTag(), "isDetached = " + isDetached());
        Log.d(getTag(), "isHidden = " + isHidden());
        Log.d(getTag(), "isRemoving = " + isRemoving());
        Log.d(getTag(), "isInBackStack = " + FragmentHelper.isInBackStack(this));

        if (getActivity() != null) {
            Log.d(getTag(), "activity isFinishing = " + getActivity().isFinishing());
            Log.d(getTag(), "activity isDestroyed = " + getActivity().isDestroyed());
        } else {
            Log.d(getTag(), "activity is null");
        }
    }

    private void printSavedState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Log.d(getTag(), "savedInstanceState is null");
        } else {
            Log.d(getTag(), "savedInstanceState = " + savedInstanceState);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // lifecycle
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void onAttach(Context context) {
        Log.i(getTag(), "onAttach");
        printState();
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i(getTag(), "onCreate");
        printState();
        printSavedState(savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(getTag(), "onCreateView");
        printState();
        printSavedState(savedInstanceState);
        return inflater.inflate(R.layout.fragment_textview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        tvTag.setText(getTag());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.i(getTag(), "onActivityCreated");
        printState();
        printSavedState(savedInstanceState);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        Log.i(getTag(), "onViewStateRestored");
        printState();
        printSavedState(savedInstanceState);
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.i(getTag(), "onStart");
        printState();
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(getTag(), "onResume");
        printState();
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(getTag(), "onPause");
        printState();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.i(getTag(), "onSaveInstanceState");
        printState();
        printSavedState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        Log.i(getTag(), "onStop");
        printState();
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i(getTag(), "onDestroyView");
        printState();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(getTag(), "onDestroy");
        printState();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.i(getTag(), "onDetach");
        printState();
        super.onDetach();
    }

}
