package com.beloo.laboratory;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LifecycleActivity extends AppCompatActivity {
    private static final String TAG = LifecycleActivity.class.getSimpleName();
    private static final String EXTRA_FRAGMENT_NUM = "fragmentNum";

    private int fragmentNum = 1;
    private Button btnState;

    private void printState() {
        Log.d(TAG, "activity isFinishing = " + isFinishing());
        Log.d(TAG, "activity isDestroyed = " + isDestroyed());
    }

    private void printSavedState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Log.d(TAG, "savedInstanceState is null");
        } else {
            Log.d(TAG, "savedInstanceState = " + savedInstanceState);
        }
    }

    private String newFragmentTag() {
        String tag = "fragment " + fragmentNum;
        fragmentNum ++;
        return tag;
    }

    @OnClick(R.id.btnAdd)
    void onAddFragmentClicked() {
        Log.w(TAG, "open new fragment");
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_container, new LifecycleFragment(), newFragmentTag())
                .addToBackStack(null)
                .commit();
    }

    @OnClick(R.id.btnRecord)
    void onRecordClicked() {
        Log.w(TAG, "close activity");
    }

    @Override
    public void onBackPressed() {
        Log.w(TAG, "onBackPressed");
        super.onBackPressed();
        fragmentNum--;
    }

    ///////////////////////////////////////////////////////////////////////////
    // lifecycle
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        printState();
        printSavedState(savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        btnState = (Button) findViewById(R.id.btnState);
        btnState.setOnClickListener(v -> v.setVisibility(View.GONE));

        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fl_container, new LifecycleFragment(), newFragmentTag())
                    .commit();
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
            if (fragment != null) {
                Log.d(TAG, "fragment is found in onCreate");
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.i(TAG, "onRestoreInstanceState");
        printState();
        printSavedState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
        fragmentNum = savedInstanceState.getInt(EXTRA_FRAGMENT_NUM, 1);
    }

    @Override
    protected void onRestart() {
        Log.i(TAG, "onRestart");
        printState();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "onStart");
        printState();
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume");
        printState();
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        printState();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.i(TAG, "onSaveInstanceState");
        printState();
        outState.putInt(EXTRA_FRAGMENT_NUM, fragmentNum);
        printSavedState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");
        printState();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        printState();
        super.onDestroy();
    }

}
