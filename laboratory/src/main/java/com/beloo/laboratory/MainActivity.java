package com.beloo.laboratory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnState = (Button) findViewById(R.id.btnState);

        btnState.setOnClickListener(v -> v.setVisibility(View.GONE));

        setContentView(R.layout.activity_main);
    }
}
