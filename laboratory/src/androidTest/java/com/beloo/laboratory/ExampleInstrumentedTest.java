package com.beloo.laboratory;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.PublishSubject;

import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.beloo.laboratory", appContext.getPackageName());
    }

    @Test
    public void subscribed_EventSynchronouslySent() throws Exception {
        //arrange
        PublishSubject<String> subject = PublishSubject.create();
        Observable<String> observable = subject;
        TestObserver<String> testObserver = TestObserver.create();
        observable.subscribe(testObserver);
        for(int i=0; i < 1000; i++) {
            subject.onNext("test");
        }
        testObserver.assertValueCount(1000);
    }

    @Test
    public void lock_and_main_thread() {

        Lock lock = new ReentrantLock();
        PublishSubject<String> subject = PublishSubject.create();
        TestObserver<String> testObserver = TestObserver.create();
        TestObserver<String> testObserver1 = TestObserver.create();

        subject.observeOn(AndroidSchedulers.mainThread())
                .firstOrError()
                .doOnSuccess(e -> lock(lock, () -> {
                    System.out.println("test");
                    return null;
                }))
                .subscribe(testObserver);

        Completable.fromAction(() -> lock(lock, () -> {
            subject.onNext("test");
            return null;
        })).subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(testObserver1);

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);

        testObserver1.awaitTerminalEvent();
        testObserver1.assertComplete();
    }

    private static <T> T lock(Lock lock, Supplier<T> action) {
        lock.lock();
        try {
            return action.get();
        } finally {
            lock.unlock();
        }
    }
}
